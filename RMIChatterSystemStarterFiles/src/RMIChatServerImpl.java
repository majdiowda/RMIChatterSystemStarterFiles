import java.util.*;
import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.server.*;

/**
 *  This class implements an RMI Chat Server.
 *  It is intended to run under RMI technology and simply connects to
 * 	any number of clients for the echoing of sent messages
 *  Methods are available to connect and send messages <p>
 *
 *  Usage Example: 
 *  <pre>
 *
 *    // connect to the remote RMIChatServer
 *	  RMIChatServer remoteChatter = (RMIChatServer) Naming.lookup(rmiUrl);
 *	  remoteChatter.connect(clientref);
 *
 *    // send a message
 *    remoteChatter.sendMessageToServer("Hello out there");
 *
 *  </pre>
 */

 /* To Do: #1. 
   ********************************************
   * Complete the class declaration.
   ********************************************
   * You will need to:
   *		- Complete the class declaration to
   *		  extend the correct class
   *		- Complete the class declaration to
   *		  implement the correct interface
   *
   *********************************************
  */
public class RMIChatServerImpl  {

	/**
	 *  The client vector. This holds one entry
	 *  for each client that has registered with
	 *  the server.
	 */
	private Vector myClientVector;

	/**
	 *  Constructs the chat server
	 */
	public RMIChatServerImpl() throws RemoteException {

		// simply instantiate the Vector
		myClientVector = new Vector();
	}

	/**
	 *  Allows the registration of a chat client
	 */
	public void connect(RMIChatClient theClient) throws RemoteException {


		/* To Do: #2.
		 *********************************************
		 * Register the Client
		 *********************************************
		 * You will need to:
		 *		- Store the passed reference in the
		 *		  client Vector
		 *
		 *********************************************
		 */



	}

	/**
	 *  Allows the sending of a message
	 */
	public void sendMessageToServer(String theMessage)
	  throws RemoteException {


		/* To Do: #3.
		 ************************************************
		 * Dispatch the supplied message
		 ************************************************
		 * You will need to:
		 *		- Iterate over the client Vector
		 *		- Send the supplied message to each
		 *  	  registered client
		 *		- Catch and log any error from the attempt
		 *		  to send
		 *
		 ************************************************
		 */



	}

	public static void main (String[] args){

		try{
			/* To Do: #4.
	 		 ********************************************
	 		 * Create an instance of the Remote Object
	 		 ********************************************
	 		 * You will need to:
	 		 *		- Create an instance of the
	 		 *		  RMIChatServerImpl class
	 		 *
			 *********************************************
			 */



			/* To Do: #5.
	 	 	 *************************************************
	 	 	 * Bind the remote object to the registry
	 	 	 *************************************************
	 	 	 * You will need to:
	 	 	 *		- Bind your RMIChatServer object
	 	 	 *		  reference to the registry. Use the
	 	 	 *		  name RMIChatServerX or any other name.
	 	 	 *
		 	 **************************************************
		 	 */



		 	System.out.println("Remote object bound to registry");
		}
		catch( Exception e){

			System.out.println("Failed to register object " + e);
			e.printStackTrace();
			System.exit(1);

		}

	}

}