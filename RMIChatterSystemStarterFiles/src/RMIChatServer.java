import java.rmi.*;
import java.util.*;

/**
 * This interface class represents the services available from a remote
 * RMI Chat Server.   <p> 
 */


/* To Do: #1. 
 * *******************************************
 * The remote interface declares the methods
 * that a client may call remotely.
 *
 * 1. Declare the RMIChatServer interface.
 *    Hint: Needs to extend the Remote interface.
 *
 * 2. Declare the following remote methods:
 *
 * 		public void connect(RMIChatClient theClient)
 *
 * 		public void sendMessageToServer(String theMessage)
 *
 *    Hint: All methods must throw the RemoteException.
 *
 *********************************************
 */
