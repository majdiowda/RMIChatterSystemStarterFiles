import java.rmi.*;
import java.util.*;

/**
 * This interface class represents the services available from a remote
 * RMI Chat Client   <p>  
 */


/* To Do: #1.
 * *******************************************
 * The remote interface declares the methods
 * that a chat client offers.
 *
 * 1. Declare the RMIChatClient interface.
 *    Hint: Needs to extend the Remote interface.
 *
 * 2. Declare the following remote methods:
 *
 * 		public void dispatchMessage(String theMessage)
 *
 *    Hint: All methods must throw the RemoteException.
 *
 *********************************************
 */
