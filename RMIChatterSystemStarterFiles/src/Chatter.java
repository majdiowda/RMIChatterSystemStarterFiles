import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.naming.*;
import java.rmi.*;

/**
 *  This class is the RMI chat GUI. It allows the
 * user to store sets of textual based data under a
 * supplied name 
 *
 */
public class Chatter extends JFrame {

	/**
	 * Reference to the Chat Server
	 */
	private RMIChatServer chatServer;

	/**
	 *  The current received message display
	 */
	private JTextArea receivedList;

	/**
	 *  Send button
	 */
	private JButton sendButton;

	/**
	 *  Connect button
	 */
	private JButton connectButton;

	/**
	 *  The current message
	 *  line to be sent
	 */
	private JTextField messageText;

	/**
	 *  This constructor creates the Chatter UI
	 */
	public Chatter() {

		//
		//  LAYOUT FRAME
		//
		setTitle("Chatter");

		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());

		receivedList = new JTextArea(20,30);
		messageText = new JTextField(30);

		sendButton = new JButton("Send");
		sendButton.setEnabled(false);
		connectButton = new JButton("Connect...");

		// layout the UI
		Panel bottomPanel = new Panel();
		bottomPanel.add(connectButton);
		bottomPanel.add(new JLabel("Enter Message"));
		bottomPanel.add(messageText);
		bottomPanel.add(sendButton);

		container.add(bottomPanel, BorderLayout.SOUTH);
		container.add(receivedList, BorderLayout.CENTER);

		pack();

		//
		//   LISTENERS
		//
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				setVisible(false);
				dispose();
				System.exit(0);
		}});

		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				connectToServer();
		}});

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				sendToServer( messageText.getText());
		}});

	}


	/**
	 *  Connects to the chat server
	 */
	public void connectToServer(){

		// ask the user for a server name
		String bindName = JOptionPane.showInputDialog(this,
							"Please give lookup name",
							"Chatter",
							JOptionPane.QUESTION_MESSAGE);

		/* To Do: #1.
		 *************************************************
		 * Lookup the remote object
		 *************************************************
		 * You will need to:
		 *		- Lookup the remote object via the
		 *		  registry the user will have supplied the
		 * 		  required lookup string via the code above.
		 * 		- Assign the reference to the chatServer
		 *		  variable
		 *		- Instantiate a RMIChatClientImpl object
		 *		  using the receivedList reference as a
		 *		  construction parameter. This will tie
		 *		  the client into the JTextArea for output
		 *		  of messages.
		 * 		- Connect to the chat server by calling
		 *		  the connect() method and passing the
		 *		  RMIChatClientObject reference
		 *
		 * Note:
		 * The chatServer variable has been declared for you
		 **************************************************
		 */




			// If you get here without any
			// exceptions being thrown then
			// enable the send button
			// to allow messages to be sent
			sendButton.setEnabled(true);

			System.out.println("Connected!");

	}

	/**
	 *  Takes the entered message and
	 *  sends it to the chat server
	 */
	public void sendToServer( String theMessage){

		/* To Do: #2.
		 *************************************************
		 * Send the message
		 *************************************************
		 * You will need to:
		 *		- Call the sendMessageToServer() method on the
		 * 		  chatServer passing the required message
		 *
		 **************************************************
		 */



		// clear the data item
		messageText.setText("");

	}

	/**
	 *	Convience method for printing system messages
	 */
	 public void log (String message) {

		 System.out.println("Chatter: " + message);
	 }

	/**
	 *  Main method to create the Chatter program and display the GUI frame.
	 */
	public static void main(String[] args)  {

		Chatter myFrame = new Chatter();

		myFrame.setVisible(true);
	}

}