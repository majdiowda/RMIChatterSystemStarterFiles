import java.util.*;
import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.server.*;
import javax.swing.*;

/**
 *  This class implements an RMI Chat Client
 *  It is intended to run under RMI technology and receives
 * 	messages from connected servers
 *  Methods are available to send messages <p>
 *
 *  Usage Example: 
 *  <pre>
 *
 *    // connect to the remote RMIChatServer
 *	  remoteChatter.connect(clientref);
 *
 *    // send a message ... at the server end
 *    clientref.dispatchMessage("Hello out there");
 *
 *  </pre>
 */

 /* To Do: #1.
   ********************************************
   * Complete the class declaration.
   ********************************************
   * You will need to:
   *		- Complete the class declaration to
   *		  extend the correct class and
   *		  implement the correct interface
   *********************************************
  */
public class RMIChatClientImpl {

	/** The reference to the JTextArea to
	 * be supplied with messages
	 */
	 JTextArea theSink;

	/**
	 *  Constructs the chat client
	 */
	public RMIChatClientImpl( JTextArea theSuppliedSink) throws RemoteException {


		/* To Do: #2.
		 ************************************************
		 * Store the TextArea reference
		 ************************************************
		 * You will need to:
		 *		- Store the passed JTextArea reference in
		 *		  the class level theSink variable
		 *
		 ************************************************
		 */


	}

	/**
	 *  Allows the sending of a message from a server to the client
	 */
	public void dispatchMessage( String theMessage) throws RemoteException {


		/* To Do: #3.
		 **************************************************
		 * Dispatch the supplied message
		 **************************************************
		 * You will need to
		 *		- Add the supplied message to the JTextArea
		 *		  whose reference is in the theSink variable
		 *
		 **************************************************
		 */ 



	}

}